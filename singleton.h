#ifndef SINGLETON_H
#define SINGLETON_H
#include <iostream>
#include <time.h>
#include <ctime>
#include <stdio.h>
using namespace std;
class singleton{
    static int a;
    static singleton* instance;
    singleton();

public:
    singleton(singleton const&) = delete;
        singleton& operator=(singleton const&) = delete;
    static singleton* getInstance();
    void print();

};

#endif // SINGLETON_H
