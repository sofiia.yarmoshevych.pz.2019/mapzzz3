#ifndef MARK_H
#define MARK_H
#include <iostream>
#include <stdio.h>

using namespace std;


class Widget {
 public:
  virtual void print_marks() = 0;
};


class Teacher_set_agree : public Widget {
 public:
  void print_marks() {
      cout<<"What mark would you like to set?"<<endl;
      int mark;
      cin>>mark;
      cout<<"The student will be notified."<<endl;

  }
};
class Teacher_all_marks : public Widget {
 public:
  void print_marks() {
      cout<<"Student's marks:"<<endl;
      cout<<"Student 1: "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<endl;
      cout<<"Student 2: "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<endl;
      cout<<"Student 2: "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<endl;

  }
};


class Student_set_agree : public Widget {
 public:
  void print_marks() {
      cout<<"The mark is "<<rand()%11<<endl;
      cout<<"Do you agree with the mark? (y/n)"<<endl;
      getchar();
      getchar();
      cout<<"The teacher will be notified."<<endl;
  }
};
class Student_all_marks : public Widget {
 public:
  void print_marks() {
       cout<<"Your marks: "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<rand()%11<<" "<<endl;
  }
};

class Factory {
 public:
  virtual Widget *set_mark_agree() = 0;
  virtual Widget *all_marks() = 0;
};


class TeacherFactory : public Factory {
 public:
  Widget *set_mark_agree() {
    return new Teacher_set_agree;
  }
  Widget *all_marks() {
    return new Teacher_all_marks;
  }
};


class StudentFactory : public Factory {
 public:
  Widget *set_mark_agree() {
    return new Student_set_agree;
  }
  Widget *all_marks() {
    return new Student_all_marks;
  }
};


class Client {
 private:
  Factory *factory;

 public:
  Client(Factory *f) {
    factory = f;
  }

  void print_marks() {
    display_window_one();
  }

  void display_window_one() {
    Widget *w[] = {
        factory->all_marks(),
        factory->set_mark_agree()

    };
    w[0]->print_marks();
    w[1]->print_marks();
  }

};



#endif // MARK_H
