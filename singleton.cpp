#include "singleton.h"
#include "data.h"
#include "mark.h"
singleton* singleton::instance=NULL;

singleton::singleton()
{
    
}

singleton *singleton::getInstance()
{
    if(instance == nullptr){
        instance = new singleton();
    }
    return instance;
}

void singleton::print()
{
    //AbstractFactory
        char user;
          Factory *factory;
        cout<<"Welcome. Are you a teacher or student? (t/s)"<<endl;
        cin>>user;


       if(user=='s'){
          factory = new StudentFactory;
        }else{
          factory = new TeacherFactory;
       }

          Client *c = new Client(factory);
          c->print_marks();


   
        //Builder
        if(user=='t'){
            FormBuilder* builder=new FormBuilder();
            Director* director=new Director(builder);

            char answer;
            cout<<"Do you want to form the data about student? (y/n)"<<endl;
   
             cin>>answer;
            if(answer=='y'){
                int action;
                cout<<"Choose the options:"<<endl;
                cout<<"1 - set only the students name"<<endl;
                cout<<"2 - set the students name and his/her attendance"<<endl;
                cout<<"3 - set the students name, his/her attendance and average mark"<<endl<<endl;

                cin.clear();
                cin>>action;
                switch(action){
                    case 1: director->Ent_name();
                    break;
                    case 2: director->Ent_attend_name();
                    break;
                    case 3: director->Ent_mark_name();
                }


                Form* formRes=builder->getResult();
                cout<<endl<<"Form created:"<<endl<<"Student name: "<<formRes->student_name;
                if(action==2)
                    cout<<"; His/her attendance: "<<formRes->days_attendance;
                if(action==3){
                    cout<<"; His/her attendance: "<<formRes->days_attendance;
                    cout<<"; Average mark: "<<formRes->average_student_mark;
                    }
                cout<<endl;
            }

            delete director;
            delete builder;
        }


}
