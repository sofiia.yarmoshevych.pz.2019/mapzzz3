#ifndef DATA_H
#define DATA_H
#include <iostream>
#include <time.h>
#include <ctime>
#include <stdio.h>

using namespace std;

class Form{
public:
    string student_name;
    int days_attendance;
    int average_student_mark;
};


class data{
public:
    virtual ~data(){}
    virtual void set_stud_name(string name)=0;
    virtual void set_day_attend(int days)=0;
    virtual void set_avg_mark(int avg_mark)=0;
};

class FormBuilder: public data{
    Form* form;
public:
    FormBuilder(){
        clearForm();
    }
    ~FormBuilder(){
        delete form;
    }
    void clearForm(){
        form=new Form();
    }
    void set_stud_name(string name){
        form->student_name=name;
    }
    void set_day_attend(int days){
        form->days_attendance=days;
    }
    void set_avg_mark(int avg_mark){
        form->average_student_mark=avg_mark;
    }
    Form* getResult(){
        Form* res=form;
        form=NULL;
        return res;
    }
};

class Director{
    FormBuilder* builder;
public:
    Director(FormBuilder* builder):builder(builder){}
    void Ent_name(){
        builder->set_stud_name(Fill_name());
    }
    void Ent_attend_name(){
        Ent_name();
        builder-> set_day_attend(Fill_attend());
    }
    void Ent_mark_name(){
        Ent_attend_name();
        builder->set_avg_mark(Fill_mark());
    }

    string Fill_name(){
        cout<<"Enter the name of student:"<<endl;
        string nam;
        cin>>nam;
        return nam;
    }
    int Fill_attend(){
        cout<<"How many days did the student attend the lecture?"<<endl;
        int day;
        cin>>day;
        return day;
    }
    int Fill_mark(){
        cout<<"Set the average mark of the student:"<<endl;
        int mark;
        cin>>mark;
        return mark;
    }
};

#endif // DATA_H
